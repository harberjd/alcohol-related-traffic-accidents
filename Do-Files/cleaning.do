/*
Jack Harber
3 February 2020

This do-file is called cleaning.do

We will be cleaning the ARIES dataset to make it manageable for analysis.
*/

clear all
version 16.0
cd "C:\Users\harberj\Desktop\Spring 2020\ECON 49900\Alcohol Sales"
capture log close
log using "C:\Users\harberj\Desktop\Spring 2020\ECON 49900\Alcohol Sales\Log Files\cleaning.log", replace

/*
First, I will be appending all of the datasets together to form one dataset
containing all accidents since Jan 01, 2014 - Nov 06, 2019
*/

use "C:\Users\harberj\Desktop\Spring 2020\ECON 49900\Alcohol Sales\Importable Data Files\Indiana Crash Data\2015crash.dta"

/*
I am destringing the variable that tells us the BAC of the individual. This only occurs in the 2015 dataset
*/

destring resultalchtxt, replace force float

/*
I am appending the rest of the datasets. For whatever reason, they decided to use different date formats for 2015, so I had to fix it.
*/

foreach x in 4 6 7 8 {
append using `"C:\Users\harberj\Desktop\Spring 2020\ECON 49900\Alcohol Sales\Importable Data Files\Indiana Crash Data\201`x'crash.dta"'
}

/*
Fixing the format of the dates
*/

gen sifdate = date(colldte, "YMD")
drop colldte
rename sifdate colldte
format colldte %td

/*
I now add the 2019 data because it did not include the indexing_number variable correctly. I have to do it myself
*/

append using "C:\Users\harberj\Desktop\Spring 2020\ECON 49900\Alcohol Sales\Importable Data Files\Indiana Crash Data\2019crash.dta"

format indexing_number %18.0g
replace indexing_number = individual_mr_record * 100 + personnmb 

drop ïindexing_number

/*
I start with 4,362,326 observations at the start. This contains all individuals involved in
a crash
*/

/*
Sorting and ordering just to look at data
*/

gsort individual_mr_record unit_mr_number persontypecde personnmb -colldte -collision_time collision_time_am_pm motorvehinvolvednmb indexing_number
order individual_mr_record unit_mr_number personnmb persontypedescr colldte collision_time collision_time_am_pm motorvehinvolvednmb indexing_number

/*
Seeing how many instances we have where people are linked to the same crash, but the time or date of the crash do not match
*/
preserve
keep if individual_mr_record == individual_mr_record[_n-1] & (colldte != colldte[_n-1] | collision_time != collision_time[_n-1])
count
*There are 30 instances
quietly by individual_mr_record : gen dup = cond(_N==1, 0, _n)
drop if dup >= 2
drop dup
count
* There are 6 collisions
restore


/*
Need to label missing persontypecde as 10 so that I can drop these values if we
have a code for the driver. Also, I drop all observations if they have are linked to their
same crash, but they occur on a different date
*/

replace persontypecde = 10 if missing(persontypecde)

/*
Next, if a vehicle in a crash is designated with a driver (which will have a persontypecde of 1, and appear first with our previous sort), I only want to keep that observation. If there is no designated driver for the vehicle in the crash, then I just keep the first and only observation for that vehicle in the crash.
*/

quietly by individual_mr_record unit_mr_number: gen dup = cond(_N==1, 0, _n)
drop if dup >= 2

drop dup 


/*
Now our samples only consists of potential drivers since we only keep one observation
per vehicle involved in crash. Now, we look at the alcohol of these potential drivers.
Here, if someone has a BAC greater than 0, then I say that they are related to alcohol.
*/

gen alch00 = 1
replace alch00 = 0 if (resultalchtxt == 0 | missing(resultalchtxt)) 

/*
Here, if someone has a BAC greater than 0.08, then I say that they are related to alcohol (robustness check)
*/

gen alch08 = 1
replace alch08 = 0 if (resultalchtxt < 0.08 | missing(resultalchtxt)) 


/*
Here, if anybody related to the collision has a BAC greater than 0, then I say everyone is related to an alcohol-related collision.
*/
	
egen alcohol = max(alch00), by(individual_mr_record)

/*
Here, if anybody related to the collisin has a BAC greater than 0.08, then I say everyone is related to an alcohol-related collision (robustness check).
*/

egen bac08 = max(alch08), by(individual_mr_record)

/*
Now, only keeping unique crashes. I do not care which ones I drop as all that I am interested in is whether or not the crash itself was related to alcohol-related.

However, there were 4,360,040 people involved in crashes across the dataset at the beginning
*/

rename countydescr county

quietly bysort individual_mr_record (county): gen dap = cond(_N==1, 0, _n)
drop if dap >= 2

/*
Now we only keep the necessary variables
*/

drop resultalchtxt unit_mr_number personnmb motorvehinvolvednmb indexing_number statuscde persontypecde persontypedescr gendercde age_grp posinvehcde posinvehdescr ejecttrapcde ejecttrapdescr safetyequuseddescr safetyequusedcde safetyequeffind injstatuscde injstatuscde injstatusdescr injnaturecde injstatusdescr injloccde injloccdescr testgivencde testgivendescr resultalchtxt agencyoritxt agencyoridescr countycde citycde citydescr trailersinvolvednmb deernmb rdwysuffixtxt rdwyramptxt interinterchangetxt incorplimitind propdamagecde propdamagedescr latdecimalnmb longdecimalnmb trafficcntlopind aggressivedriveind hitrunind schoolzoneind constructind lightcondcde lightconddescr weathercde surfacetypecde surfacetypedescr primaryfactorcde primaryfactordescr mannercolldescr mannercollcde timenotifiedtxt timenotifiedampmtxt timearrivedtxt timearrivedampmtxt investcompleteind photostakenind uniquelocationid statepropind trafficcntrlcde trafficcntrldescr unitnmb unit_vehicle_number unittypecde unittypedescr vehyeartxt vehmaketxt vehmodeltxt occupsnmb vehlicstatecde vehlicstatedescr axelstxt speedlimittxt towedind vehusecde vehusedescr roadtypecde roadtypedescr travdircde travdirdescr emgerency_run fireind colleventcde colleventdescr precollactcde precollactdescr district district_num subdistrict injnaturedescr resultdrug resultdrugind rumblestripind dap alch00 alch08 collision_day collision_month collision_year weatherdescr surfacetypecde_conddescr 


/*
Figuring out how to change the date and time so that we can create our own definition of days to begin at 12:00 pm on a day and end at 12:00 pm the following day.
*/

rename collision_time time
rename collision_time_am_pm am_pm
replace am_pm = lower(am_pm)
replace time = time + " " + am_pm
gen double crashtime = clock(time, "hm")
format crashtime %tcHH:MM:SS
gen double datetime = colldte*24*60*60*1000 + crashtime
format datetime %tcDay_Mon_DD_HH:MM:SS_CCYY

sort colldte datetime county alcohol  
order colldte datetime county alcohol 

/*
Here, we are trying to attribute the crash to the correct day. Thus, if the crash occurs on or after 12:00 pm and before 12:00 pm the following day, then that crash occurred on the day in question.
*/

gen double crashdate = datetime - msofhours(24) if hh(datetime) < 12
replace crashdate = datetime if missing(crashdate)
replace crashdate = crashdate/(24*60*60*1000)
format crashdate %td
gen year = year(crashdate)
gen month = month(crashdate)
gen day = day(crashdate)

gen newdate = mdy(month, day, year)
format newdate %td
drop crashdate
rename newdate crashdate

/*
Here we find the day of the week associated with the adjusted date
*/

gen weekday = dow(crashdate)

/*
I drop Dec 31, 2013 and Nov 6, 2019 since they do not have all the crashes corresponding to their day
*/

drop if crashdate < td(01jan2014) | crashdate > td(05nov2019)
drop colldte time am_pm datetime injurednmb deadnmb crashtime 

rename county_state countynmb

/*
We need to know which county the crash occurred in
*/
drop if county == ""

/*
Here is just data cleaning. I am trying to make the data look nicer
*/

label define lweekday 0 "Sunday" 1 "Monday" 2 "Tuesday" 3 "Wednesday" 4 "Thursday" 5 "Friday" 6 "Saturday"
label define lalcohol 0 "Not Alcohol-related" 1 "Alcohol-related"

label values weekday lweekday
label values alcohol lalcohol

*Need to rename some county names to match later for a merge
replace county = "DeKalb" if county == "De Kalb"
replace county = "StJoseph" if county == "St. Joseph"
replace county = "StJoseph" if county == "St Joseph"

*If in one of these counties, then it is a border county
gen border = 1 if (inlist(county, "Adams", "Allen", "Benton", "Clark", "Crawford", "DeKalb", "Dearborn", "Elkhart", "Floyd") | inlist(county, "Franklin", "Gibson", "Harrison", "Jay", "Jefferson", "Knox", "Lake", "LaPorte", "LaGrange") | inlist(county, "Newton", "Ohio", "Perry", "Posey", "Randolph", "StJoseph", "Steuben", "Switzerland") | inlist(county, "Spencer", "Sullivan", "Union", "Wayne", "Warrick", "Vanderburgh", "Vigo", "Vermillion", "Warren"))
replace border = 0 if border != 1

label define lborder 0 "Nonborder" 1 "Border"

label values border lborder

*I 
gen noban = 1 if crashdate >= td(04mar2018)
replace noban = 0 if missing(noban)

label define lpolicy 0 "Before Repeal of Ban" 1 "After Repeal of Ban"

label values noban lpolicy

/*
Now calculating more variables
*/

*Grouping observations that had the same crash date
egen sameday = group(crashdate)

/*
Finding the total amount of alcohol-related crashes on that date and separating by whether or not it is a border county or nonborder county. Then looking at alcohol-related fatalities as well as alcohol-related injuries
*/

*Alcohol-related crashes
egen alcoholcrashes00_by_county_type = total(alcohol), by(sameday border)
egen alcoholcrashes00 = total(alcohol), by(sameday)
egen alcoholcrashes00_bycounty = total(alcohol), by(sameday county)

/*
Doing the same exact thing, but looking at if the BAC >= 0.08
*/
egen alcoholcrashes08_by_county_type = total(bac08), by(sameday border)
egen alcoholcrashes08 = total(bac08), by(sameday)
egen alcoholcrashes08_bycounty = total(alcohol), by(sameday county)

/*
Now trying to find nonalcohol-related crashes for BAC > 0.00 and BAC >= 0.08
*/
egen totalcrash_by_county_type = count(crashdate), by(sameday border)
egen totalcrashes = count(crashdate), by(sameday)
egen totalcrash_bycounty = count(crashdate), by(sameday county)
gen totalnonalcoholcrash00 = totalcrashes - alcoholcrashes00
gen totalnonalcoholcrash08 = totalcrashes - alcoholcrashes08
gen totalnonalcoholcrash00_bycounty = totalcrash_bycounty - alcoholcrashes00_bycounty
gen totalnonalcoholcrash08_bycounty = totalcrash_bycounty - alcoholcrashes08_bycounty
gen nonalcoholcrash00_bycountytype = totalcrash_by_county_type - alcoholcrashes00_by_county_type
gen nonalcoholcrash08_bycountytype = totalcrash_by_county_type - alcoholcrashes08_by_county_type

/*
Summary statistics
*/

*Number of crashes in Border Counties
count if border == 1

*Number of crashes in Nonborder Counties
count if border == 0

*Number of alcohol-related crashes total
count if alcohol == 1 
count if bac08 == 1

/*
Here, I keep a maximum of 92 (bc of number of counties) per day. Some days had no crashes for a county, so it appears that the observations for these states are missing. I will have to add in observations to ensure that each day has 92 observations, even if the number of crashes is zero.
*/

sort crashdate county border
drop if crashdate == crashdate[_n-1] & county == county[_n-1]

drop totalcrashes totalcrash_bycounty totalcrash_by_county_type alcohol bac08 individual_mr_record

/*
Here, I am trying to add the observations for county so that the data has 2135 days for each county. To do this, I create a tempfile that achieves this. I then merge this new dataset with the previous dataset, and this gives me 2135 days per county. For the days that are added due to the merge, the total crashes for these days are 0.
*/

preserve

clear all

tempfile missing
set obs 196420
gen sameday = ceil(_n / 92)
gen countynmb = 1
replace countynmb = countynmb[_n-1] + 1 if sameday == sameday[_n-1]
save `missing'

restore

merge 1:1 sameday countynmb using `missing'
drop _merge

********************************************************************************
********************************************************************************
********************************************************************************

/*
Adding values in key variables for the new observations created. These variables have the same value for a given day. 
*/

sort sameday countynmb border
foreach var in crashdate alcoholcrashes00 alcoholcrashes08 year month day weekday totalnonalcoholcrash00 totalnonalcoholcrash08 {
	replace `var' = `var'[_n-1] if sameday == sameday[_n-1] & missing(`var')
	replace `var' = `var'[_n+1] if sameday == sameday[_n+1] & missing(`var')
}

/*
Here, I am naming the county names based on the countynmb
*/

local num = 1
foreach place in Adams Allen Bartholomew Benton Blackford Boone Brown Carroll Cass Clark Clay Clinton Crawford Daviess Dearborn Decatur DeKalb Delaware Dubois Elkhart Fayette Floyd Fountain Franklin Fulton Gibson Grant Greene Hamilton Hancock Harrison Hendricks Henry Howard Huntington Jackson Jasper Jay Jefferson Jennings Johnson Knox Kosciusko LaGrange Lake LaPorte Lawrence Madison Marion Marshall Martin Miami Monroe Montgomery Morgan Newton Noble Ohio Orange Owen Parke Perry Pike Porter Posey Pulaski Putnam Randolph Ripley Rush StJoseph Scott Shelby Spencer  Starke Steuben Sullivan Switzerland Tippecanoe Tipton Union Vanderburgh Vermillion Vigo Wabash Warren Warrick Washington Wayne Wells White Whitley {
	replace county = `"`place'"' if countynmb == `num' & county == ""
	local num = `num' + 1
}

/*
I once again define what is a border vs nonborder county
*/

replace border = 1 if (inlist(county, "Adams", "Allen", "Benton", "Clark", "Crawford", "DeKalb", "Dearborn", "Elkhart", "Floyd") | inlist(county, "Franklin", "Gibson", "Harrison", "Jay", "Jefferson", "Knox", "Lake", "LaPorte", "LaGrange") | inlist(county, "Newton", "Ohio", "Perry", "Posey", "Randolph", "StJoseph", "Steuben", "Switzerland") | inlist(county, "Spencer", "Sullivan", "Union", "Wayne", "Warrick", "Vanderburgh", "Vigo", "Vermillion", "Warren"))
replace border = 0 if border != 1

/*
Here, these variables have a value of 0 if the observation came from the merged dataset
*/

foreach r in alcoholcrashes00_bycounty alcoholcrashes08_bycounty totalnonalcoholcrash00_bycounty totalnonalcoholcrash08_bycounty {
	replace `r' =  0 if missing(`r')
}

replace noban = 1 if crashdate >= td(04mar2018)
replace noban = 0 if missing(noban)

gsort sameday border -alcoholcrashes00_by_county_type

/*
All border counties on the sameday will have the same values for these variables.
I don't have to use sameday == sameday[_n-1] since it is sorted by sameday first
*/

foreach u in alcoholcrashes08_by_county_type alcoholcrashes00_by_county_type nonalcoholcrash00_bycountytype nonalcoholcrash08_bycountytype {
	replace `u' = `u'[_n-1] if missing(`u') & border == border[_n-1]
}

********************************************************************************
********************************************************************************
********************************************************************************

/*
Merging the gas prices database
*/

merge m:1 month year using "C:\Users\harberj\Desktop\Spring 2020\ECON 49900\Alcohol Sales\Importable Data Files\Indiana Crash Data\gasprices.dta"
drop _merge 

/*
Creating holiday variables
*/

gen daylightsavings = 1 if (month == 3 & ((year == 2014 & day == 9) | (year == 2015 & day == 8) | (year == 2016 & day == 13) | (year == 2017 & day == 12) | (year == 2018 & day == 11) | (year == 2019 & day == 10))) | (month == 11 & ((year == 2014 & day == 2) | (year == 2015 & day == 1) | (year == 2016 & day == 6) | (year == 2017 & day == 5) | (year == 2018 & day == 4) | (year == 2019 & day == 3)))
replace daylightsavings = 0 if daylightsavings != 1

gen superbowl = 1 if month == 2 & ((year == 2014 & day == 2) | (year == 2015 & day == 1) | (year == 2016 & day == 7) | (year == 2017 & day == 5) | (year == 2018 & day == 4) | (year == 2019 & day == 3))
replace superbowl = 0 if superbowl != 1

gen stpatricks = 1 if month == 3 & day == 17
replace stpatricks = 0 if stpatricks != 1

gen cincodemayo = 1 if month == 5 & day == 5
replace cincodemayo = 0 if cincodemayo != 1

gen memorialdaywknd = 1 if month == 5 & ((year == 2014 & (day == 26 | day == 25 | day == 24)) | (year == 2015 & (day == 25 | day == 24 | day == 23)) | (year == 2016 & (day == 30 | day == 29 | day ==28)) | (year == 2017 & (day == 29 | day == 28 | day == 27)) |(year == 2018 & (day == 28 | day == 27 | day ==26)) | (year == 2019 & (day == 27 | day == 26 | day == 25)))
replace memorialdaywknd = 0 if memorialdaywknd != 1

gen labordaywknd = 1 if (((month == 8 & (day == 30 | day == 31)) | (month == 9 & day == 1)) & year == 2014) | (month == 9 & (day == 5 | day == 6 | day == 7) & year == 2015) | (month == 9 & (day == 3 | day == 4 | day == 5) & year == 2016) | (month == 9 & (day == 2 | day == 3 | day == 4) & year == 2017) | (month == 9 & (day == 1 | day == 2 | day == 3) & year == 2018) | (((month == 8 & (day == 30)) | (month == 9 & (day == 1 | day == 2))) & year == 2019)
replace labordaywknd = 0 if labordaywknd != 1

gen independence = 1 if (month == 7 & day == 4)
replace independence = 0 if independence != 1

gen independenceeve = 1 if (month == 7 & day == 3)
replace independenceeve = 0 if independenceeve != 1

gen halloween = 1 if month == 10 & day == 31
replace halloween = 0 if halloween != 1

gen thanksgivingeve = 1 if month == 11 & ((year == 2014 & day == 26) | (year == 2015 & day == 25) | (year == 2016 & day == 23) | (year == 2017 & day == 22) | (year == 2018 & day == 21))
replace thanksgivingeve = 0 if thanksgivingeve != 1

gen thanksgiving = 1 if month == 11 & ((year == 2014 & day == 27) | (year == 2015 & day == 26) | (year == 2016 & day == 24) | (year == 2017 & day == 23) | (year == 2018 & day == 22))
replace thanksgiving = 0 if thanksgiving != 1

gen christmas = 1 if month == 12 & day == 25
replace christmas = 0 if christmas != 1

gen newyearseve = 1 if (month == 12 & day == 31)
replace newyearseve = 0 if newyearseve != 1 

gen holiday = 1 if (daylightsavings == 1 | superbowl == 1 | stpatricks == 1 | cincodemayo == 1 | memorialdaywknd == 1 | labordaywknd == 1 | independenceeve == 1 | independence == 1 | halloween == 1 | thanksgivingeve == 1 | thanksgiving == 1 | christmas == 1 | newyearseve == 1 )
replace holiday = 0 if missing(holiday)

drop daylightsavings superbowl stpatricks cincodemayo memorialdaywknd labordaywknd independenceeve independence halloween thanksgivingeve thanksgiving christmas newyearseve

/*
Creating variables for the summary statistics
*/

local a 0
foreach x in sunday monday tuesday wednesday thursday friday saturday {
	gen `x' = 1 if weekday == `a'
	replace `x' = 0 if weekday != `a'
	local a = `a' + 1
}

label variable sunday "Sunday"
label variable monday "Monday"
label variable tuesday "Tuesday"
label variable wednesday "Wednesday"
label variable thursday "Thursday"
label variable friday "Friday"
label variable saturday "Saturday"

gen nonborder = border - 1 if border == 1
replace nonborder = 1 if border == 0
label define lnonborder 0 "Border" 1 "Nonborder"
label values nonborder lnonborder

/*
More summary statistics
*/

count if weekday == 0 & noban == 0 & countynmb == 2
*There are 217 Sundays prior to the law change
count if weekday == 0 & noban == 1 & countynmb == 2
*There are 88 Sundays after the law change

*Saving file
save "C:\Users\harberj\Desktop\Spring 2020\ECON 49900\Alcohol Sales\Cleaning Do-File\clean.dta", replace

capture log close
