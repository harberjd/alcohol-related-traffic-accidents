/*
Jack Harber
8 April 2020

This do-file is called results.do

We will be conducting our results in this do-file.
*/

/*
We first get summary statistics of our key variable, alcohol-related crashes
*/

clear all
version 16.0
cd "C:\Users\harberj\Desktop\Spring 2020\ECON 49900\Alcohol Sales"
capture log close
log using "C:\Users\harberj\Desktop\Spring 2020\ECON 49900\Alcohol Sales\Log Files\results.log", replace

/*
Using the file
*/

use "C:\Users\harberj\Desktop\Spring 2020\ECON 49900\Alcohol Sales\Cleaning Do-File\clean.dta"

/*
Conducting summary statistics
*/

estpost tabstat alcoholcrashes00 if countynmb == 2, by(weekday) statistics(mean sd count min max median) columns(statistics) 
esttab, cells("count mean(fmt(2)) sd(fmt(2)) min max p50(fmt(2))") 
eststo: esttab, cells("count mean(fmt(2)) sd(fmt(2)) min max p50(fmt(2))") 
esttab est1 using sumindianacrash.tex, noobs modelwidth(10 20) cell((count(label(Count)) mean(label(Mean)fmt(2)) sd(par label(Standard Deviation)fmt(2)) min(label(Minimum)) max(label(Maximum)) p50(label(Median)))) label title(All Indiana - Alcohol-Related Crashes) nonumber replace

estpost tabstat alcoholcrashes00_by_county_type if countynmb == 2, by(weekday) statistics(mean sd count min max median) columns(statistics) 
esttab, cells("count mean(fmt(2)) sd(fmt(2)) min max p50(fmt(2))") 
eststo: esttab, cells("count mean(fmt(2)) sd(fmt(2)) min max p50(fmt(2))") 
esttab est2 using sumbordercrash.tex, noobs modelwidth(10 20) cell((count(label(Count)) mean(label(Mean)fmt(2)) sd(par label(Standard Deviation)fmt(2)) min(label(Minimum)) max(label(Maximum)) p50(label(Median)))) label title(Border Counties - Alcohol-Related Crashes) nonumber replace

estpost tabstat alcoholcrashes00_by_county_type if countynmb == 3, by(weekday) statistics(mean sd count min max median) columns(statistics) 
esttab, cells("count mean(fmt(2)) sd(fmt(2)) min max p50(fmt(2))") 
eststo: esttab, cells("count mean(fmt(2)) sd(fmt(2)) min max p50(fmt(2))") 
esttab est3 using sumnonbordercrash.tex, noobs modelwidth(10 20) cell((count(label(Count)) mean(label(Mean)fmt(2)) sd(par label(Standard Deviation)fmt(2)) min(label(Minimum)) max(label(Maximum)) p50(label(Median)))) label title(Nonborder Counties - Alcohol-Related Crashes) nonumber replace

/*
Creating key variables
*/

gen sunday_noban = sunday*noban
label variable sunday_noban "Sunday*Repeal" 
gen nonborder_sunday = nonborder * sunday

gen monday_noban = monday*noban
label variable monday_noban "Monday*Repeal" 

gen tuesday_noban = tuesday*noban
label variable tuesday_noban "Tuesday*Repeal" 

gen wednesday_noban = wednesday*noban
label variable wednesday_noban "Wednesday*Repeal" 

gen thursday_noban = thursday*noban
label variable thursday_noban "Thursday*Repeal" 

gen friday_noban = friday*noban
label variable friday_noban "Friday*Repeal" 

gen saturday_noban = saturday*noban
label variable saturday_noban "Saturday*Repeal" 

/*
In every regression, I am utilizing county fixed effects, so the dependent variable is the number of alcohol-related crashes in a county
*/

sort sameday countynmb

/*
We run our regressions
*/
********************************************************************************
********************************************************************************
********************************************************************************

/*
Model 1 - Indiana in Aggregate, Nonborder Counties, and Border Counties
*/

/*
This is model 1
*/


xtset countynmb sameday

foreach x in 00 08 {

di "Poisson - All Indiana"
xtpoisson alcoholcrashes`x'_bycounty i.noban holiday i.weekday i.month nonalcoholcrash`x'_bycounty gasprice sameday, irr vce(robust) fe

eststo model1poisson_all_`x'

di "Poisson - Nonborder Counties"
xtpoisson alcoholcrashes`x'_bycounty i.noban holiday i.weekday i.month nonalcoholcrash`x'_bycounty gasprice sameday if nonborder == 1, irr vce(robust) fe

eststo model1poisson_nb_`x'

di "Poisson - Border Counties"
xtpoisson alcoholcrashes`x'_bycounty i.noban holiday i.weekday i.month nonalcoholcrash`x'_bycounty gasprice sameday if nonborder == 0, irr vce(robust) fe

eststo model1poisson_b_`x'

di "OLS - All Indiana"
xtreg alcoholcrashes`x'_bycounty i.noban holiday i.weekday i.month nonalcoholcrash`x'_bycounty gasprice sameday, vce(robust) fe

eststo model1ols_all_`x'

di "OLS - Nonborder Counties"
xtreg alcoholcrashes`x'_bycounty i.noban holiday i.weekday i.month nonalcoholcrash`x'_bycounty gasprice sameday if nonborder == 1, vce(robust) fe

eststo model1ols_nb_`x'

di "OLS - Border Counties"
xtreg alcoholcrashes`x'_bycounty i.noban holiday i.weekday i.month nonalcoholcrash`x'_bycounty gasprice sameday if nonborder == 0, vce(robust) fe

eststo model1ols_b_`x'

********************************************************************************
********************************************************************************
********************************************************************************

/*
This is model 2
*/


/*
Sunday Models
*/

********
*Sunday vs All Days
********

di "Sunday vs All Days - Poisson - All Indiana"
xtpoisson alcoholcrashes`x'_bycounty i.sunday##i.noban holiday i.month nonalcoholcrash`x'_bycounty gasprice sameday, irr vce(robust) fe

eststo poisson_sunall_all_`x'

di "Sunday vs All Days - Poisson - Nonborder Counties"
xtpoisson alcoholcrashes`x'_bycounty i.sunday##i.noban holiday i.month nonalcoholcrash`x'_bycounty gasprice sameday if nonborder == 1, irr vce(robust) fe

eststo poisson_sunall_nb_`x'

di "Sunday vs All Days - Poisson - All Indiana"
xtpoisson alcoholcrashes`x'_bycounty i.sunday##i.noban holiday i.month nonalcoholcrash`x'_bycounty gasprice sameday if nonborder == 0, irr vce(robust) fe

eststo poisson_sunall_b_`x'

di "Sunday vs All Days - OLS - All Indiana"
xtreg alcoholcrashes`x'_bycounty i.sunday##i.noban holiday i.month nonalcoholcrash`x'_bycounty gasprice sameday, vce(robust) fe

eststo ols_sunall_all_`x'

di "Sunday vs All Days - OLS - Nonborder Counties"
xtreg alcoholcrashes`x'_bycounty i.sunday##i.noban holiday i.month nonalcoholcrash`x'_bycounty gasprice sameday if nonborder == 1, vce(robust) fe

eststo ols_sunall_nb_`x'

di "Sunday vs All Days - OLS - All Indiana"
xtreg alcoholcrashes`x'_bycounty i.sunday##i.noban holiday i.month nonalcoholcrash`x'_bycounty gasprice sameday if nonborder == 0, vce(robust) fe

eststo ols_sunall_b_`x'

********
*Sunday vs Weekdays
********

di "Sunday vs Weekdays - Poisson - All Indiana"
xtpoisson alcoholcrashes`x'_bycounty i.sunday##i.noban holiday i.month nonalcoholcrash`x'_bycounty gasprice sameday if weekday != 5 & weekday != 6, irr vce(robust) fe

eststo poisson_sunwkdys_all_`x'

di "Sunday vs Weekdays - Poisson - Nonborder Counties"
xtpoisson alcoholcrashes`x'_bycounty i.sunday##i.noban holiday i.month nonalcoholcrash`x'_bycounty gasprice sameday if nonborder == 1 & weekday != 5 & weekday != 6, irr vce(robust) fe

eststo poisson_sunwkdys_nb_`x'

di "Sunday vs Weekdays - Poisson - Border Counties"
xtpoisson alcoholcrashes`x'_bycounty i.sunday##i.noban holiday i.month nonalcoholcrash`x'_bycounty gasprice sameday if nonborder == 0 & weekday != 5 & weekday != 6, irr vce(robust) fe

eststo poisson_sunwkdys_b_`x'

di "Sunday vs Weekdays - OLS - All Indiana"
xtreg alcoholcrashes`x'_bycounty i.sunday##i.noban holiday i.month nonalcoholcrash`x'_bycounty gasprice sameday if weekday != 5 & weekday != 6, vce(robust) fe

eststo ols_sunwkdys_all_`x'

di "Sunday vs Weekdays - OLS - Nonborder Counties"
xtreg alcoholcrashes`x'_bycounty i.sunday##i.noban holiday i.month nonalcoholcrash`x'_bycounty gasprice sameday if nonborder == 1 & weekday != 5 & weekday != 6, vce(robust) fe

eststo ols_sunwkdys_nb_`x'

di "Sunday vs Weekdays - OLS - Border Counties"
xtreg alcoholcrashes`x'_bycounty i.sunday##i.noban holiday i.month nonalcoholcrash`x'_bycounty gasprice sameday if nonborder == 0 & weekday != 5 & weekday != 6, vce(robust) fe

eststo ols_sunwkdys_b_`x'

********
*Sunday vs Weekends
********

di "Sunday vs (Friday & Saturday) - Poisson - All Indiana"
xtpoisson alcoholcrashes`x'_bycounty i.sunday##i.noban holiday i.month nonalcoholcrash`x'_bycounty gasprice sameday if (weekday == 5 | weekday == 6 | weekday == 0), irr vce(robust) fe

eststo poisson_sunwknd_all_`x'

di "Sunday vs (Friday & Saturday) - Poisson - Nonborder Counties"
xtpoisson alcoholcrashes`x'_bycounty i.sunday##i.noban holiday i.month nonalcoholcrash`x'_bycounty gasprice sameday if nonborder == 1 & (weekday == 5 | weekday == 6 | weekday == 0), irr vce(robust) fe

eststo poisson_sunwknd_nb_`x'

di "Sunday vs (Friday & Saturday) - Poisson - All Indiana"
xtpoisson alcoholcrashes`x'_bycounty i.sunday##i.noban holiday i.month nonalcoholcrash`x'_bycounty gasprice sameday if nonborder == 0 & (weekday == 5 | weekday == 6 | weekday == 0), irr vce(robust) fe

eststo poisson_sunwknd_b_`x'

di "Sunday vs (Friday & Saturday) - OLS - All Indiana"
xtreg alcoholcrashes`x'_bycounty i.sunday##i.noban holiday i.month nonalcoholcrash`x'_bycounty gasprice sameday if (weekday == 5 | weekday == 6 | weekday == 0), vce(robust) fe

eststo ols_sunwknd_all_`x'

di "Sunday vs (Friday & Saturday) - OLS - Nonborder Counties"
xtreg alcoholcrashes`x'_bycounty i.sunday##i.noban holiday i.month nonalcoholcrash`x'_bycounty gasprice sameday if nonborder == 1 & (weekday == 5 | weekday == 6 | weekday == 0), vce(robust) fe

eststo ols_sunwknd_nb_`x'

di "Sunday vs (Friday & Saturday) - OLS - All Indiana"
xtreg alcoholcrashes`x'_bycounty i.sunday##i.noban holiday i.month nonalcoholcrash`x'_bycounty gasprice sameday if nonborder == 0 & (weekday == 5 | weekday == 6 | weekday == 0), vce(robust) fe

eststo ols_sunwknd_b_`x'

********
*All Other Days
********
 
	foreach y in mon tues wednes thurs fri satur{ 
di `"Poisson - All Indiana - `y'day"'
xtpoisson alcoholcrashes`x'_bycounty i.`y'day##i.noban holiday i.month nonalcoholcrash`x'_bycounty gasprice sameday, irr vce(robust) fe

eststo poisson_`y'_all_`x'

di `"Poisson - Nonborder Counties - `y'day"'
xtpoisson alcoholcrashes`x'_bycounty i.`y'day##i.noban holiday i.month nonalcoholcrash`x'_bycounty gasprice sameday if nonborder == 1, irr vce(robust) fe

eststo poisson_`y'_nb_`x'

di `"Poisson - Border Counties - `y'day"'
xtpoisson alcoholcrashes`x'_bycounty i.`y'day##i.noban holiday i.month nonalcoholcrash`x'_bycounty gasprice sameday if nonborder == 0, irr vce(robust) fe

eststo poisson_`y'_b_`x'

di `"OLS - All Indiana - `y'day"'
xtreg alcoholcrashes`x'_bycounty i.`y'day##i.noban holiday i.month nonalcoholcrash`x'_bycounty gasprice sameday, vce(robust) fe

eststo ols_`y'_all_`x'

di `"OLS - Nonborder Counties - `y'day"'
xtreg alcoholcrashes`x'_bycounty i.`y'day##i.noban holiday i.month nonalcoholcrash`x'_bycounty gasprice sameday if nonborder == 1, vce(robust) fe

eststo ols_`y'_nb_`x'

di `"OLS - Border Counties - `y'day"'
xtreg alcoholcrashes`x'_bycounty i.`y'day##i.noban holiday i.month nonalcoholcrash`x'_bycounty gasprice sameday if nonborder == 0, vce(robust) fe

eststo ols_`y'_b_`x'
	}


/*
This is model 3
*/

/*
Looking at border vs nonborder only on Sundays
*/

preserve
keep if sunday == 1

di "Model 3 - Poisson - Nonborder vs Border on Sunday"
xtpoisson alcoholcrashes`x'_bycounty i.nonborder##i.noban holiday i.month nonalcoholcrash`x'_bycounty gasprice sameday, vce(robust) irr fe

eststo model3poisson_`x'

di "Model 3 - OLS - Nonborder vs Border on Sunday"
xtreg alcoholcrashes`x'_bycounty i.nonborder##i.noban holiday i.month nonalcoholcrash`x'_bycounty gasprice sameday, vce(robust) fe

eststo model3ols_`x'
restore

}


********************************************************************************
********************************************************************************
********************************************************************************
/* LaTeX code time */
********************************************************************************
********************************************************************************
********************************************************************************


/*
Here I am exporting the LaTeX code
*/

*Table 1: Looking at Noban (BAC > 0.00) - Model 1
esttab model1ols_all_00 model1ols_nb_00 model1ols_b_00 model1poisson_all_00 model1poisson_nb_00 model1poisson_b_00 using table1.tex, b(3) se(3) label star(* 0.10 ** 0.05 *** 0.01) title("The Effect of Repeal on Total Alcohol-related Crashes") eform keep(1.noban) nonotes addnotes("Robust standard errors in parentheses. The exponeniated coefficients are reported." "Dependent variable is number of alcohol-related crashes. *** p $<$ 0.01, ** p $<$ 0.05, * p $<$ 0.1.") replace

*Table 2: Looking at Noban (BAC >= 0.00) - Model 1
esttab model1ols_all_08 model1ols_nb_08 model1ols_b_08 model1poisson_all_08 model1poisson_nb_08 model1poisson_b_08 using table2.tex, b(3) se(3) label star(* 0.10 ** 0.05 *** 0.01) title("The Effect of Repeal on Total Alcohol-related Crashes") eform keep(1.noban) nonotes addnotes("Robust standard errors in parentheses. The exponeniated coefficients are reported." "Dependent variable is number of alcohol-related crashes. *** p $<$ 0.01, ** p $<$ 0.05, * p $<$ 0.1.") replace

*Looking at Sunday-Specifically*
*Table 3: First Indiana in Aggregate (BAC > 0.00) - Model 2
esttab ols_sunall_all_00 ols_sunwkdys_all_00 ols_sunwknd_all_00 poisson_sunall_all_00 poisson_sunwkdys_all_00 poisson_sunwknd_all_00 using table3.tex,  b(3) se(3) label star(* 0.10 ** 0.05 *** 0.01) title("The Effect of Repeal on Sunday Alcohol-related Crashes in Indiana in Aggregate") eform keep(1.sunday#1.noban) nonotes addnotes("Robust standard errors in parentheses. The exponeniated coefficients are reported." "Dependent variable is number of alcohol-related crashes. *** p $<$ 0.01, ** p $<$ 0.05, * p $<$ 0.1.") replace

*Table 4: First Indiana in Aggregate (BAC >= 0.08) - Model 2
esttab ols_sunall_all_08 ols_sunwkdys_all_08 ols_sunwknd_all_08 poisson_sunall_all_08 poisson_sunwkdys_all_08 poisson_sunwknd_all_08 using table4.tex,  b(3) se(3) label star(* 0.10 ** 0.05 *** 0.01) title("The Effect of Repeal on Sunday Alcohol-related Crashes in Indiana in Aggregate") eform keep(1.sunday#1.noban) nonotes addnotes("Robust standard errors in parentheses. The exponeniated coefficients are reported." "Dependent variable is number of alcohol-related crashes. *** p $<$ 0.01, ** p $<$ 0.05, * p $<$ 0.1.") replace

*Table 5: Second Nonborder Counties (BAC > 0.00) - Model 2
esttab ols_sunall_nb_00 ols_sunwkdys_nb_00 ols_sunwknd_nb_00 poisson_sunall_nb_00 poisson_sunwkdys_nb_00 poisson_sunwknd_nb_00 using table5.tex,  b(3) se(3) label star(* 0.10 ** 0.05 *** 0.01) title("The Effect of Repeal on Sunday Alcohol-related Crashes in Indiana in Aggregate") eform keep(1.sunday#1.noban) nonotes addnotes("Robust standard errors in parentheses. The exponeniated coefficients are reported." "Dependent variable is number of alcohol-related crashes. *** p $<$ 0.01, ** p $<$ 0.05, * p $<$ 0.1.") replace

*Table 6: Second Nonborder Counties (BAC >= 0.08) - Model 2
esttab ols_sunall_nb_08 ols_sunwkdys_nb_08 ols_sunwknd_nb_08 poisson_sunall_nb_08 poisson_sunwkdys_nb_08 poisson_sunwknd_nb_08 using table6.tex,  b(3) se(3) label star(* 0.10 ** 0.05 *** 0.01) title("The Effect of Repeal on Sunday Alcohol-related Crashes in Indiana in Aggregate") eform keep(1.sunday#1.noban) nonotes addnotes("Robust standard errors in parentheses. The exponeniated coefficients are reported." "Dependent variable is number of alcohol-related crashes. *** p $<$ 0.01, ** p $<$ 0.05, * p $<$ 0.1.") replace

*Table 7: Third Border Counties (BAC > 0.00) - Model 2
esttab ols_sunall_b_00 ols_sunwkdys_b_00 ols_sunwknd_b_00 poisson_sunall_b_00 poisson_sunwkdys_b_00 poisson_sunwknd_b_00 using table7.tex,  b(3) se(3) label star(* 0.10 ** 0.05 *** 0.01) title("The Effect of Repeal on Sunday Alcohol-related Crashes in Indiana in Aggregate") eform keep(1.sunday#1.noban) nonotes addnotes("Robust standard errors in parentheses. The exponeniated coefficients are reported." "Dependent variable is number of alcohol-related crashes. *** p $<$ 0.01, ** p $<$ 0.05, * p $<$ 0.1.") replace

*Table 8: Third Border Counties (BAC >= 0.08) - Model 2
esttab ols_sunall_b_08 ols_sunwkdys_b_08 ols_sunwknd_b_08 poisson_sunall_b_08 poisson_sunwkdys_b_08 poisson_sunwknd_b_08 using table8.tex,  b(3) se(3) label star(* 0.10 ** 0.05 *** 0.01) title("The Effect of Repeal on Sunday Alcohol-related Crashes in Indiana in Aggregate") eform keep(1.sunday#1.noban) nonotes addnotes("Robust standard errors in parentheses. The exponeniated coefficients are reported." "Dependent variable is number of alcohol-related crashes. *** p $<$ 0.01, ** p $<$ 0.05, * p $<$ 0.1.") replace

*Looking at Incidence Ratios for each - Model 2
local a = 9
foreach y in mon tues wednes thurs fri satur { 

di `"`y'day Incident Ratios (BAC > 0.00)"'
esttab ols_`y'_all_00 ols_`y'_nb_00 ols_`y'_b_00 poisson_`y'_all_00 poisson_`y'_nb_00 poisson_`y'_b_00 using table`a'.tex, b(3) ci(3) label star(* 0.10 ** 0.05 *** 0.01) title("Incident Rates") eform keep(1.`y'day#1.noban) nonotes addnotes("Robust standard errors in parentheses. The exponeniated coefficients are reported." "Dependent variable is number of alcohol-related crashes. *** p $<$ 0.01, ** p $<$ 0.05, * p $<$ 0.1.") replace

local a = `a' + 1

di `"`y'day Incident Ratios (BAC >= 0.08)"'
esttab ols_`y'_all_08 ols_`y'_nb_08 ols_`y'_b_08 poisson_`y'_all_08 poisson_`y'_nb_08 poisson_`y'_b_08 using table`a'.tex, b(3) ci(3) label star(* 0.10 ** 0.05 *** 0.01) title("Incident Rates") eform keep(1.`y'day#1.noban) nonotes addnotes("Robust standard errors in parentheses. The exponeniated coefficients are reported." "Dependent variable is number of alcohol-related crashes. *** p $<$ 0.01, ** p $<$ 0.05, * p $<$ 0.1.") replace

local a = `a' + 1
}

*Table 21: Nonborder vs Border Counties (BAC > 0.00)
esttab model3ols_00 model3poisson_00 using table21.tex, b(3) se(3) label star(* 0.10 ** 0.05 *** 0.01) title("Nonborder vs Border") eform keep(1.nonborder#1.noban) nonotes addnotes("Robust standard errors in parentheses. The exponeniated coefficients are reported." "Dependent variable is number of alcohol-related crashes. *** p $<$ 0.01, ** p $<$ 0.05, * p $<$ 0.1.") replace

*Table 22: Nonborder vs Border Counties (BAC >= 0.08)
esttab model3ols_08 model3poisson_08 using table22.tex, b(3) se(3) label star(* 0.10 ** 0.05 *** 0.01) title("Nonborder vs Border") eform keep(1.nonborder#1.noban) nonotes addnotes("Robust standard errors in parentheses. The exponeniated coefficients are reported." "Dependent variable is number of alcohol-related crashes. *** p $<$ 0.01, ** p $<$ 0.05, * p $<$ 0.1.") replace


capture log close







