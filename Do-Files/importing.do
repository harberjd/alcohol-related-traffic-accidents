/*
Jack Harber
1 February 2020

This do-file is called importing.do

We are importing CSV files and converting these into DTA files
for Stata. The converted files will be save in the folder Importable Data Files.
These files contain crash data for 2017, 2018, and 2019 in the state of Indiana
*/

clear all
version 16.0
cd "C:\Users\harberj\Desktop\Spring 2020\ECON 49900\Alcohol Sales"
capture log close
log using "C:\Users\harberj\Desktop\Spring 2020\ECON 49900\Alcohol Sales\Log Files\importing.log", replace

/*
First, importing 2014 crash data
*/

import delimited "C:\Users\harberj\Desktop\Spring 2020\ECON 49900\Alcohol Sales\Original Data Files\Indiana Crash Data\aries_crash_data_2014.csv"

save "C:\Users\harberj\Desktop\Spring 2020\ECON 49900\Alcohol Sales\Importable Data Files\Indiana Crash Data\2014crash.dta", replace
clear all

/*
Next, importing 2015 crash data
*/

import delimited "C:\Users\harberj\Desktop\Spring 2020\ECON 49900\Alcohol Sales\Original Data Files\Indiana Crash Data\aries_crash_data_2015.csv"

save "C:\Users\harberj\Desktop\Spring 2020\ECON 49900\Alcohol Sales\Importable Data Files\Indiana Crash Data\2015crash.dta", replace
clear all

/*
Next, importing 2016 crash data
*/
import delimited "C:\Users\harberj\Desktop\Spring 2020\ECON 49900\Alcohol Sales\Original Data Files\Indiana Crash Data\aries_crash_data_2016.csv"

save "C:\Users\harberj\Desktop\Spring 2020\ECON 49900\Alcohol Sales\Importable Data Files\Indiana Crash Data\2016crash.dta", replace
clear all


/*
Next, importing 2017 crash data
*/

import delimited "C:\Users\harberj\Desktop\Spring 2020\ECON 49900\Alcohol Sales\Original Data Files\Indiana Crash Data\aries_crash_data_2017.csv"

save "C:\Users\harberj\Desktop\Spring 2020\ECON 49900\Alcohol Sales\Importable Data Files\Indiana Crash Data\2017crash.dta", replace
clear all

/*
Next, importing 2018 crash data
*/

import delimited "C:\Users\harberj\Desktop\Spring 2020\ECON 49900\Alcohol Sales\Original Data Files\Indiana Crash Data\aries_crash_data_2018.csv"

save "C:\Users\harberj\Desktop\Spring 2020\ECON 49900\Alcohol Sales\Importable Data Files\Indiana Crash Data\2018crash.dta", replace
clear all

/*
Next, importing 2019 crash data
*/
import delimited "C:\Users\harberj\Desktop\Spring 2020\ECON 49900\Alcohol Sales\Original Data Files\Indiana Crash Data\aries_crash_data_2019.csv"

gen sifdate = date(colldte, "MDY")
drop colldte
rename sifdate colldte
format colldte %td

save "C:\Users\harberj\Desktop\Spring 2020\ECON 49900\Alcohol Sales\Importable Data Files\Indiana Crash Data\2019crash.dta", replace
clear all

/*
Importing our transposed crash dictionary
*/

import excel "C:\Users\harberj\Desktop\Spring 2020\ECON 49900\Alcohol Sales\Original Data Files\Indiana Crash Data\transposed_aries_crash_data_data_dictionary.xlsx", sheet("Sheet1") firstrow allstring

save "C:\Users\harberj\Desktop\Spring 2020\ECON 49900\Alcohol Sales\Importable Data Files\Indiana Crash Data\crashdictionary.dta", replace

clear all

*Now importing monthly gas prices

import delimited "C:\Users\harberj\Desktop\Spring 2020\ECON 49900\Alcohol Sales\Original Data Files\Indiana Crash Data\Indiana Total Gasoline Wholesale per Resale Price by Refiners.csv", varnames(5)

rename indianatotalgasolinewholesaleres gasprice
gen date = monthly(month, "MY", 2050)
replace date = dofm(date)
drop month
gen month = month(date)
gen year = year(date)

keep if year == 2014 | year == 2015 | year == 2016 | year == 2017 | year ==2018 | (year == 2019 & month != 12)

drop date

save "C:\Users\harberj\Desktop\Spring 2020\ECON 49900\Alcohol Sales\Importable Data Files\Indiana Crash Data\gasprices.dta", replace

capture log close